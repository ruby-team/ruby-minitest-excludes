# Replace this by the standard debian/ruby-tests.rake on a release that has
# https://github.com/seattlerb/minitest-excludes/issues/3 fixed
task :default do
  Dir.chdir("debian/tests") do
    ruby "test/test.rb", "--verbose"
  end
end
