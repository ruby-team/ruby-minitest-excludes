require 'minitest/autorun'
require 'minitest/excludes'

class ATestCase < Minitest::Test
  def test_test1; assert true  end
  def test_test2; assert false end # oh noes!
  def test_test3; assert true  end

  class Nested < Minitest::Test
    def test_test1; assert true  end
    def test_test2; assert false end # oh noes!
    def test_test3; assert true  end
  end
end

